# Ansible role: gitlab_docker

Made for **Northamp**.

## Requirements

* Ansible 2.x on a Debian-based system.
* Docker

Tested with Ansible 2.9 on Debian 10 with Python 3 configured as interpreter.

## Description

This role creates a Docker container running Gitlab CE Omnibus. It is supposed to be exposed by a third party reverse proxy (i.e. Traefik).

This role also creates a systemctl service named `disable-thp.service` that disables Transparent Huge Pages on system startup.

Please note that the Gitlab configuration have been tuned to run on a very low performance server. Change the `GITLAB_OMNIBUS_CONFIG` env var in the tasks as needed for your own setup.

## Variables

Please note that variables that have ***SENSITIVE*** in their description should be declared in an Ansible Vault.

| Name | Description | Example |
| -- | -- | -- |
| `timezone` | Timezone used by the container | `Europe/Paris` |
| `gitlab_image` | Gitlab image from hub.docker.com | `gitlab/gitlab-ce:13.2.1-ce.0` |
| `gitlab_hostname` | Gitlab hostname | `git.example.com` |
| `gitlab_registry_hostname` | Gitlab Registry hostname | `registry.git.example.com` |
| `gitlab_smtp_enable` | Enable SMTP (true) or not (false) | `false` |
| `gitlab_smtp_address` | SMTP address | `smtp.example.com` |
| `gitlab_smtp_port` | SMTP Port | `587` |
| `gitlab_smtp_user` | ***SENSITIVE*** SMTP User | `user` |
| `gitlab_smtp_password` | ***SENSITIVE*** SMTP Password | `password` |
| `gitlab_smtp_domain` | SMTP Domain | `example.com` |
| `gitlab_smtp_from` | From address | `gitlab@example.com` |
| `gitlab_smtp_replyto` | Replyto address | `noreply@example.com` |
| `gitlab_conf_vol` | Mount point of Gitlab's configuration volume | `/srv/gitlab/etc` | 
| `gitlab_log_vol` | Mount point of Gitlab's logging volume | `/srv/gitlab/log` |
| `gitlab_data_vol` | Mount point of Gitlab's data volume | `/srv/gitlab/data` |
| `gitlab_network` | Docker network used to expose Gitlab | `web` |

## Dependencies

None

## Example Playbook

TODO

## License

MIT